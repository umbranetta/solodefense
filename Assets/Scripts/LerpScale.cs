﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]

public class LerpScale : MonoBehaviour
{
    public float smoothVal;
    public Vector3 origScale;
    public Vector3 currentScale;
    public Player player;

    void Start()
    {
        origScale = transform.localScale;
    }
    
    void Update()
    {
        //ScaleLerp();
        if (Input.GetKeyDown(KeyCode.A))
        {
            Scale(1,15f);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
             Scale(0,currentScale.x);
        }
    }

   
   

    IEnumerator ScaleFluid(Vector3 a,Vector3 b, float time)
    {
        float i = 0;
        float rate = 1 / time;

        Vector3 fromScale = transform.localScale;
        Vector3 toScale = new Vector3(transform.localScale.x, 1.5f, transform.localScale.z);
        while (i < 1)
        {
            i += Time.deltaTime * rate;
            transform.localScale = Vector3.Lerp(a, b, i);
            yield return 0;
        }
    }

    public void Scale(int direction,float value)
    {
        Vector3 toScale = new Vector3(value, transform.localScale.y, transform.localScale.z);
        currentScale = toScale;
       // Vector3 toZeroScale = new Vector3(transform.localScale.x,0.1f, transform.localScale.z);
        if (direction==1)
        {
           
            StartCoroutine(ScaleFluid(origScale, toScale, 1f));
        }
        else if (direction==0)
        {
           // Vector3 toScale = new Vector3(transform.localScale.x, 1.5f, transform.localScale.z);
            StartCoroutine(ScaleFluid(currentScale, origScale, 1f));
        }
       
    }

    public IEnumerator PowerUps()
    {
         Scale(1,15f);
         yield return new WaitForSeconds(10f);
        Scale(0,currentScale.x);
        player.isOnPower = false;
    }

    
    
}
