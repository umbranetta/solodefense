﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //public GameObject enemyPrefs;
    public List<Transform> waypoints;
    public List<GameObject> spawnedEnemy;
    public ObjectPool objectPool;
    GameObject enem;

    
    [SerializeField]
    float spawnRadius;
    public static float time ;
    float defaultTime = 3f;
    public float timeReference;
    void Start()
    {
      time = defaultTime;
      timeReference = time;
        //StartCoroutine(SpawnEnemy());
        //objectPool = ObjectPool.Instance;
    }

    // Update is called once per frame
    void Update()
    {
      timeReference = time;
    }
 
   public IEnumerator SpawnEnemy()
    {
       // Vector2 spawnPos =  GameObject.FindGameObjectWithTag("Base").transform.position;
        //spawnPos += Random.insideUnitCircle.normalized * spawnRadius;
        var e = Random.Range(0,waypoints.Count);
       enem =  objectPool.SpawnPrefabs("q",waypoints[e].position,Quaternion.identity);
        
      // GameObject enem =  Instantiate(enemyPrefs,waypoints[e].position,Quaternion.identity);
      
        //   var g = enem.GetComponent<Enemy>();

        //   if (time == 2)
        // {
        //     var rand = Random.Range(2f, 4f);
        //     g.speed+=3f;
        // }else if (time == 1)
        // {
        //     var rand = Random.Range(4f, 6f);
        //     g.speed+=5f;
        // }else
        // {
        //     var rand = Random.Range(1f, 2f);
        //     g.speed+=rand;
        // }
      
       
       
      // spawnedEnemy.Add(enem);
        yield return new WaitForSeconds(time);
        StartCoroutine(SpawnEnemy());
    }

    public void DestroySpawnedEnemy()
    {
        // foreach (var item in spawnedEnemy)
        // {         
        //     var e = item;   
        //     spawnedEnemy.Remove(e);           
        //     Destroy(e);
        // }

        for (int i = 0; i < spawnedEnemy.Count; i++)
        {
            var e = spawnedEnemy[i];
           // spawnedEnemy.Remove(e);
            Destroy(e);
        }
    }

    public void SetToDefaultTime()
    {
      time = defaultTime;
    }
}
