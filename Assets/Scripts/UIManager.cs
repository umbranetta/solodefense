﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    
    public TMP_Text currentScore;
    public TMP_Text bestScore;
    public TMP_Text healthCount;
    public Button retryBtn;
    public Button secondChanceBtn;
    public Button quitBtn;
    public Button exitBtn;
    public Button playBtn;
    public CanvasGroup gameOverPanel;
    ScoreManager scoreManager;
    void Start()
    {
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
    }

    // Update is called once per frame
    void Update()
    {
        currentScore.text ="Score : " +""+ ScoreManager.currentScore.ToString();
        bestScore.text = "Best Score : " +""+  scoreManager.bestScore.ToString();

        
    }
}
