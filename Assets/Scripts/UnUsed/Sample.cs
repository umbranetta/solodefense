﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Sample : MonoBehaviour
{
    public static bool test;
    public GameObject panel;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.T))
        {
            test = true;
        }

        if (Input.GetKeyDown(KeyCode.V))
        {
            panel.SetActive(false);
        }

        if (Input.GetKeyDown(KeyCode.G))
        {
           var e = SceneManager.GetActiveScene().buildIndex;
           SceneManager.LoadScene(e);
           Debug.Log("scene restart");
        }
    }
}
