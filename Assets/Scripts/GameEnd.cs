﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnd : MonoBehaviour
{
   UIManager uI;
   public static int counter = 0;
   public static bool isContinue;
   public int referenceCounter;

    private void Start()
    {
        referenceCounter = counter;

        uI = GameObject.FindGameObjectWithTag("UIManager").GetComponent<UIManager>();
        uI.retryBtn.onClick.AddListener(OnClickRetry);
      //  uI.secondChanceBtn.onClick.AddListener(OnClickSecondChance);
        uI.quitBtn.onClick.AddListener(OnClickQuit);
        InitGameOverPanel();
    }

    private void Update()
    {
         referenceCounter = counter;
    }
    public void OnClickRetry()
    {
        isContinue = false;
        GameManager.Instance.enemySpawner.SetToDefaultTime();
        GameManager.hideMain = true;
        AudioManager.instance.PlaySound("Click");
        ScoreManager.currentScore = 0;
        GameManager.Instance.PlayerStartPos();
         RestartLevel();
    }

    public void OnClickSecondChance()
    {
        isContinue = true;
        AudioManager.instance.PlaySound("UseHealth");
        RestartLevel();
        GameManager.Instance.PlayerStartPos();
        // if (ExtraLifeManager.instance.healthCount !=0)
        // {
        //     ExtraLifeManager.instance.healthCount -=1;
        //     PlayerPrefs.SetInt("Health",ExtraLifeManager.instance.healthCount);
        // }
        
    }

    public void OnClickQuit()
    {
        EnemySpawner.time = 3f;
        ScoreManager.currentScore = 0;
        AudioManager.instance.PlaySound("Click");
        SceneManager.LoadScene(0);
        
        InitGameOverPanel();
    }

    void RestartLevel()
    {
        var e = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(e);
        
        StartCoroutine(GameManager.Instance.enemySpawner.SpawnEnemy());
       // InitGameOverPanel();
       // GameManager.Instance.tower.InitHealth();
        GameManager.Instance.oneTime = false;
    }

    public void InitGameOverPanel()
    {
        uI.gameOverPanel.alpha = 0;
        uI.gameOverPanel.blocksRaycasts = false;
        uI.gameOverPanel.interactable = false;
    }

    public void OnGameOver()
    {
        // AudioManager.instance.PlaySound("GameOver");
        // uI.gameOverPanel.alpha = 1f;
        // uI.gameOverPanel.blocksRaycasts = true;
        // uI.gameOverPanel.interactable = true;

        StartCoroutine(GameOver2());
    }

    IEnumerator GameOver2()
    {
        yield return new WaitForSeconds(1f);
        counter++;
        AudioManager.instance.PlaySound("GameOver");
        uI.gameOverPanel.alpha = 1f;
        uI.gameOverPanel.blocksRaycasts = true;
        uI.gameOverPanel.interactable = true;
    }

    
}
