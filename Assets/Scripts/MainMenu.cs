﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Button playBtn;
    public Button exitBtn;
    void Start()
    {
        
        playBtn.onClick.AddListener(OnClickPlay);
        exitBtn.onClick.AddListener(OnExit);
        StartCoroutine(DelayBG());
    }

    public void OnClickPlay()
    {
         AudioManager.instance.PlaySound("Click");
        // GameManager.Instance.ShowInGameMenu();
        // //GameManager.Instance.tower.InitHealth();
        // GameManager.Instance.isGameStart = true;
        // StartCoroutine(GameManager.Instance.enemySpawner.SpawnEnemy());
        SceneManager.LoadScene(1);
    }

    public void OnExit()
    {
        Application.Quit();
    }

    IEnumerator DelayBG()
    {
        yield return new WaitForSeconds(.1f);
        AudioManager.instance.PlaySound("BG");
    }
}
