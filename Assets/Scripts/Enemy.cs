﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Transform target;
    
    public float speed;
    

     ScoreManager scoreManager;
     EnemySpawner enemySpawner;
     public bool isMoving;
   
    void Start()
    {
      
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
        enemySpawner = GameObject.FindGameObjectWithTag("EnemySpawner").GetComponent<EnemySpawner>();
        target = GameObject.FindGameObjectWithTag("Base").transform;
    }

    private void OnEnable()
    {
          isMoving = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isMoving)
        {
            transform.position = Vector3.MoveTowards(transform.position,target.position,speed *Time.deltaTime);
        }

        // if (enemySpawner.time == 2f)
        // {
        //      var rand = Random.Range(2f, 4f);
        //      speed = rand;
        // }else if (enemySpawner.time == 2f)
        // {
        //     var rand = Random.Range(2f, 4f);
        //      speed = rand;
        // }else
        // {
        //     var rand = Random.Range(2f, 4f);
        //      speed = rand;
        // }


        if (EnemySpawner.time == 2f)
        {
             var rand = Random.Range(2f, 4f);
             speed = rand;
        }else if (EnemySpawner.time == 2f)
        {
            var rand = Random.Range(2f, 4f);
             speed = rand;
        }else
        {
            var rand = Random.Range(2f, 4f);
             speed = rand;
        }
        

       
    }

    private void OnCollisionEnter(Collision other) 
    {
        if (other.gameObject.tag.Equals("Base"))
        {
            GameManager.Instance.hit.OnHit(other);
            AudioManager.instance.PlaySound("BaseHit");
            var e = other.gameObject.GetComponent<Base>();
            var dmg = Random.Range(10f,15f);
            e.TakeDamage(dmg);
            GameManager.Instance.enemySpawner.spawnedEnemy.Remove(gameObject);
            //Destroy(gameObject);
            this.gameObject.SetActive(false);
            
        }
        // if (other.gameObject.tag.Equals("Damage"))
        // {
        //     Destroy(gameObject);
        //     scoreManager.OnScore(1);
        // }   
    }
}
