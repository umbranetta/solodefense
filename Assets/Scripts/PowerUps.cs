﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    public float waitTime;
   private void OnEnable()
   {
      StartCoroutine(Hide());
       
   }

   private void Update()
   {
      Rotate();
   }

   IEnumerator Hide()
   {
      yield return new WaitForSeconds(waitTime);
      gameObject.SetActive(false);
   }

   void Rotate()
   {
      transform.Rotate(0,45f*Time.deltaTime,0);
   }
}
