﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnRateController : MonoBehaviour
{
    public float timeDelay;
   public float lastIncreased;
   public float timeSinceStart;
   public static float refToTime;
    // Start is called before the first frame update
    void Start()
    {
        if (GameEnd.isContinue)
        {
            timeSinceStart = refToTime;  
        }else
        {
            timeSinceStart = 0;
        }
         
        //lastIncreased = Time.timeSinceLevelLoad;
    }

    // Update is called once per frame
    void Update()
    {
         
         if (GameEnd.isContinue)
         {
              timeSinceStart+= Time.deltaTime+refToTime;
         }else
         {
             timeSinceStart+= Time.deltaTime;
         }
        
            // if (Time.time > timeDelay + lastIncreased && GameManager.Instance.enemySpawner.time != 1f)
            // {
            //     lastIncreased = Time.time;
            //     GameManager.Instance.enemySpawner.time-=1f;
            // }

            if (timeSinceStart > timeDelay + lastIncreased && EnemySpawner.time != 1f)
            {
                //lastIncreased = Time.time;
                refToTime = timeSinceStart;
                lastIncreased = timeSinceStart;
                EnemySpawner.time -=1f;
            }
        
       
    }
}
