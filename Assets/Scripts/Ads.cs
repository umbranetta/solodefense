﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.UI;
public class Ads : MonoBehaviour,IUnityAdsListener
{
    string googleId = "3632450";
       
    public Button myButton;
    public string myPlacementId = "rewardedVideo";
    string placementId = "video";
    public static Ads instance;

    private void Awake()
    {
        // if (instance == null)
        // {
        //     instance = this;
        // }else
        // {
        //     Destroy(gameObject);
        //     return;
        // }
        // DontDestroyOnLoad(gameObject);
    }
    void Start () {   
        //myButton = GetComponent <Button> ();

        // Set interactivity to be dependent on the Placement’s status:
        myButton.interactable = Advertisement.IsReady (myPlacementId); 

        // Map the ShowRewardedVideo function to the button’s click listener:
        if (myButton) myButton.onClick.AddListener (ShowRewardedVideo);

        // Initialize the Ads listener and service:
        Advertisement.AddListener (this);
        Advertisement.Initialize (googleId, false);
        
    }

    public void ShowAd()
    {
        Advertisement.Show(placementId);
    }

    // Implement a function for showing a rewarded video ad:
    void ShowRewardedVideo () {
        Advertisement.Show (myPlacementId);
    }

    // Implement IUnityAdsListener interface methods:
    public void OnUnityAdsReady (string placementId) {
        // If the ready Placement is rewarded, activate the button: 
        if (placementId == myPlacementId) {        
            myButton.interactable = true;
        }
    }

    public void OnUnityAdsDidFinish (string placementId, ShowResult showResult) {
        // Define conditional logic for each ad completion status:
        if (showResult == ShowResult.Finished) {
            if (placementId == myPlacementId) { 
            // ExtraLifeManager.instance.healthCount+=1;
            // PlayerPrefs.SetInt("Health",ExtraLifeManager.instance.healthCount);
            GameManager.Instance.gameEnd.OnClickSecondChance();
            AudioManager.instance.PlaySound("FinishedAds");
            }
            // Reward the user for watching the ad to completion.
        } else if (showResult == ShowResult.Skipped) {
            if (placementId == myPlacementId)
            {
                GameManager.Instance.gameEnd.OnClickRetry();
            }
            
            // Do not reward the user for skipping the ad.
        } else if (showResult == ShowResult.Failed) {
           // Debug.LogWarning ("The ad did not finish due to an error.");
        }
    }

    public void OnUnityAdsDidError (string message) {
        // Log the error.
    }

    public void OnUnityAdsDidStart (string placementId) {
        // Optional actions to take when the end-users triggers an ad.
    } 
   

    // Update is called once per frame
   
}
