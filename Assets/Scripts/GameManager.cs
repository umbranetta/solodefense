﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    Transform spawnPt;
    public HitVfx hit;
    Transform player;
    public GameEnd gameEnd;
    public UIManager uIManager;
    public EnemySpawner enemySpawner;
    public Base tower;

    public bool gameOver;

    
    public GameObject InGameMenu;

    //public static int health = 2;
   
    // for beating high score sfx
    public bool oneTime;

    // bool for spawning enemy
    public bool isGameStart;
    public static bool hideMain;
    
    private static GameManager instance;
    public static GameManager Instance{get{return instance;}}

    private void Awake()
    {
        if (instance!= null && instance != this)
        {
            Destroy(this.gameObject);
        }else
        {
            instance = this;
        }
        
    }

    private void Start()
    {
        
        
        spawnPt = GameObject.FindGameObjectWithTag("SpawnPt").transform;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        PlayerStartPos();
       // uIManager.healthCount.text = ""+healthCount.ToString();
       StartCoroutine(enemySpawner.SpawnEnemy());
        AudioManager.instance.PlaySound("BG");
       
       
    }

    
    private void Update()
    {
        
       // uIManager.healthCount.text = ""+ExtraLifeManager.instance.healthCount.ToString();
        // if (ExtraLifeManager.instance.healthCount == 0)
        // {
        //     ExtraLifeManager.instance.healthCount = 0;
        //     uIManager.secondChanceBtn.interactable = false;
        // }else
        // {
        //      uIManager.secondChanceBtn.interactable = true;
        // }
    }

    public void ShowMainMenu()
    {

        InGameMenu.SetActive(false);
       
    }

    public void ShowInGameMenu()
    {
        InGameMenu.SetActive(true);
       
    }

    public void PlayerStartPos()
    {
        player.transform.position = spawnPt.transform.position;
    }
}
