﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;

public class ObjectPool : MonoBehaviour
{
    [System.Serializable]
    public class ObjectToPool
    {
        public string tag;
        public GameObject prefab;
        public int size;
    }
    private static ObjectPool instance;
    public static ObjectPool Instance{get{return instance;}}
    public List<ObjectToPool> pools;
    public Dictionary <string, Queue<GameObject>> poolDictionary;

    void Awake()
    {
        if (instance!= null && instance != this)
        {
            Destroy(this.gameObject);
        }else
        {
            instance = this;
        }

        
    }
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();
       StartCoroutine(DelayInit());
    }


    IEnumerator DelayInit()
    {
        yield return new WaitForSeconds(.5f);

        
        foreach (ObjectToPool pool in pools)
        {
            Queue<GameObject> poolQue = new Queue<GameObject>();
            for (int i = 0; i < pool.size; i++)
            {
                GameObject obj = Instantiate(pool.prefab);
                obj.SetActive(false);
                poolQue.Enqueue(obj);
                
            }

            poolDictionary.Add(pool.tag,poolQue);
        }
    }

    public GameObject  SpawnPrefabs(string name,Vector3 position,Quaternion rotation)
    {
        
        if (!poolDictionary.ContainsKey( name))
        {
            Debug.Log("pool with tag"+name+"dont exist");
            return null;
        }
        var objToSpawn = poolDictionary[name].Dequeue();
       
         objToSpawn.SetActive(true);
        objToSpawn.transform.position = position;
        objToSpawn.transform.rotation = rotation;
       

        poolDictionary[name].Enqueue(objToSpawn);
       return objToSpawn;
    }
}
