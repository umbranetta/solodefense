﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitVfx : MonoBehaviour
{
    public GameObject hitPref;
    public GameObject hitVfxEnemy;

   
    public void OnHit(Collision other)
    {
            ContactPoint contact = other.GetContact(0);
    
           // var g = Instantiate(hitPref,contact.point,Quaternion.identity);
             var e = VFXPool.Instance.SpawnPrefabs("explosion",contact.point,Quaternion.identity);
            StartCoroutine(DelayDisable(e,1.5f));
           // Destroy(g,1.5f);
        
    }

    public void OnHitEnemy(Collision other)
    {
            ContactPoint contact = other.GetContact(0);
    
           // var g = Instantiate(hitVfxEnemy,contact.point,Quaternion.identity);
            var e = VFXPool.Instance.SpawnPrefabs("hit",contact.point,Quaternion.identity);
            //Destroy(g,1f);
            StartCoroutine(DelayDisable(e,1f));
        
    }

    IEnumerator DelayDisable(GameObject obj,float sec)
    {
            yield return new WaitForSeconds(sec);
            obj.SetActive(false);
    }
}
