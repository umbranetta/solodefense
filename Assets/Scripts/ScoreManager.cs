﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
   public static int currentScore;
   public int bestScore;
   bool oneTime;

   private void Start()
   {
      bestScore =  PlayerPrefs.GetInt("BestScore");    
   }

   private void Update()
   {
     if (currentScore > bestScore)
     {
         bestScore = currentScore;
         if (!GameManager.Instance.oneTime)
         {
             AudioManager.instance.PlaySound("HighScore");
             GameManager.Instance.oneTime = true;
         }
         
         PlayerPrefs.SetInt("BestScore",bestScore);
     }    
   }
   public void OnScore(int socre)
   {
       currentScore+= socre;
   } 
    
}
