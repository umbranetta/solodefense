﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody r;
    public Camera myCam;
    public Transform boxTrans;
    float distanceFromCamera;
    public bool canDrag;
    
    void Start()
    {
        distanceFromCamera = Vector3.Distance(boxTrans.position,myCam.transform.position);
        r = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnMouseDrag()
    {

            r.useGravity = true;
            Vector3 pos = Input.mousePosition;
            pos.z = distanceFromCamera;
            pos = myCam.ScreenToWorldPoint(pos);
            //r.velocity = (pos - boxTrans.position) * 15f;
            r.velocity = new Vector3(pos.x - boxTrans.position.x,0,pos.z - boxTrans.position.z)*5f * Time.deltaTime;
        
        
        
    }
}
