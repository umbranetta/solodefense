﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotSpeed;
    public float angle;

    public bool rotateTo;
    
    public Transform point;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (rotateTo)
        {
            //transform.RotateAround(point.transform.position  ,point.transform.up,rotSpeed*Time.deltaTime);
            transform.position = point.position;
        }else
        {
             transform.Rotate(0,rotSpeed*Time.deltaTime,0);
        }
      
        
    }
}
