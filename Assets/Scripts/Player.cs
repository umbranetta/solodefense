﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public Rigidbody r ;
    public float force;
    ScoreManager scoreManager;
    public LerpScale lerpScale;
    public LootTable loot;
    public bool isOnPower;
   
    // Start is called before the first frame update
    void Start()
    {
        scoreManager = GameObject.FindGameObjectWithTag("ScoreManager").GetComponent<ScoreManager>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag.Equals("Enemy"))
        {
           // var rb = other.gameObject.GetComponent<Rigidbody>();
            var enem = other.gameObject.GetComponent<Enemy>();
            enem.isMoving = false;
           // rb.AddForce( transform.forward + transform.up * 100f,ForceMode.Impulse);
            GameManager.Instance.hit.OnHitEnemy(other);
            AudioManager.instance.PlaySound("EnemyHit");
            loot.GenerateLoot(other.gameObject.transform.position,Quaternion.identity);
            //Destroy(other.gameObject);
            other.gameObject.SetActive(false);
            scoreManager.OnScore(1);
        }   
    }

    private void OnTriggerEnter(Collider other)
   {
        if (other.tag.Equals("PowerUps") && !isOnPower)
        {
             isOnPower = true;
            StartCoroutine(lerpScale.PowerUps());
            other.gameObject.SetActive(false);
        }    
   }

    // private void OnParticleCollision(GameObject other)
    // {
    //     if (other.gameObject.tag.Equals("Enemy"))
    //     {
    //         //GameManager.Instance.hit.OnHitEnemy(other);
    //         AudioManager.instance.PlaySound("EnemyHit");
    //         Destroy(other.gameObject);
    //         scoreManager.OnScore(1);
    //     }   
    // }

    
}
