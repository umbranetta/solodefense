﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Base : MonoBehaviour
{
    public NonRewardAds ads;
    public float health;
    public float startHealth;
    public Image healtBar;
    void Start()
    {
        health = startHealth;
    }

    // Update is called once per frame
   public void TakeDamage(float amount)
   {
       health-= amount;
       healtBar.fillAmount = health / startHealth;

       if (health <= 0)
       {
           
           health = 0;
           
           //Ads.instance.ShowAd();
            if (!GameManager.Instance.gameOver)
            {
                OnGameOver();
                GameManager.Instance.gameOver = true;
            }
       }
   }

    void OnGameOver()
   {
       if (GameEnd.counter == 2)
       {
           ads.ShowAd();
           GameEnd.counter = 0;
       }
            
     GameManager.Instance.gameEnd.OnGameOver();
     GameManager.Instance.enemySpawner.StopAllCoroutines();
     GameManager.Instance.enemySpawner.DestroySpawnedEnemy();
     
            Debug.Log("Game Over");
       
       
   }

   public void InitHealth()
   {
       health = startHealth;
       healtBar.fillAmount = health / startHealth;
   }
}
